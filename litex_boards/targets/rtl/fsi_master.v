// © 2020 - 2021 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

// =============================================================================================
// Memory Map:
// =============================================================================================
// Device ID string, word 1
// Device ID string, word 2
// Slave ID / address / data length register, {1'b0, slave_id, address, 6'b0, data_length}
// Control register, {ipoll_enable_slave_id, 3'b0, enable_ipoll, 2'b0, enable_irq, enable_relative_address, enable_extended_address_mode, enable_crc_protection, enhanced_error_recovery, internal_cmd_issue_delay, 6'b0, data_direction, start_cycle}
// Status register, {debug_port, 8'b0, ipoll_error, 4'b0, cycle_error, 7'b0, cycle_complete}
// TX data (32 bits)
// RX data (32 bits)
// IRQ status / DMA control register, {4'b0, dma_control_field, 8'b0, interrupt_field}

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module fsi_master_litex_shim(
		// Wishbone slave port signals
		input wire slave_wb_cyc,
		input wire slave_wb_stb,
		input wire slave_wb_we,
		input wire [29:0] slave_wb_addr,
		input wire [31:0] slave_wb_dat_w,
		output wire [31:0] slave_wb_dat_r,
		input wire [3:0] slave_wb_sel,
		output wire slave_wb_ack,
		output wire slave_wb_err,

		output wire irq_o,

		output wire fsi_data_out,
		input wire fsi_data_in,
		output wire fsi_data_direction,			// 0 == tristate (input), 1 == driven (output)
		output wire fsi_clock_out,			// Must be inverted at the edge driver -- rising clocks are in reference to this signal, not the electrically inverted signal on the FSI bus

                input wire peripheral_reset,
                input wire peripheral_clock
	);

	// Control and status registers
	wire [63:0] device_id;
	wire [31:0] device_version;
	reg [31:0] sid_adr = 0;
	reg [31:0] control_reg = 0;
	wire [31:0] status_reg;

	reg [31:0] tx_data = 0;
	wire [31:0] rx_data;
	wire [31:0] dma_irq_reg;

	// Device identifier
	assign device_id = 64'h7c5250544653494d;
	assign device_version = 32'h00010000;

	reg slave_wb_ack_reg = 0;
	reg [31:0] slave_wb_dat_r_reg = 0;

	assign slave_wb_ack = slave_wb_ack_reg;
	assign slave_wb_dat_r = slave_wb_dat_r_reg;

	parameter MMIO_TRANSFER_STATE_IDLE = 0;
	parameter MMIO_TRANSFER_STATE_TR01 = 1;

	reg [31:0] mmio_buffer_address_reg = 0;
	reg [7:0] mmio_transfer_state = 0;
	reg [31:0] mmio_cfg_space_tx_buffer = 0;
	reg [31:0] mmio_cfg_space_rx_buffer = 0;

	// Wishbone connector
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			// Reset Wishbone interface / control state machine
			slave_wb_ack_reg <= 0;

			mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
		end else begin
			case (mmio_transfer_state)
				MMIO_TRANSFER_STATE_IDLE: begin
					// Compute effective address
					mmio_buffer_address_reg[31:2] = slave_wb_addr;
					case (slave_wb_sel)
						4'b0001: mmio_buffer_address_reg[1:0] = 0;
						4'b0010: mmio_buffer_address_reg[1:0] = 1;
						4'b0100: mmio_buffer_address_reg[1:0] = 2;
						4'b1000: mmio_buffer_address_reg[1:0] = 3;
						4'b1111: mmio_buffer_address_reg[1:0] = 0;
						default: mmio_buffer_address_reg[1:0] = 0;
					endcase

					if (slave_wb_cyc && slave_wb_stb) begin
						// Configuration register space access
						// Single clock pulse signals in deasserted state...process incoming request!
						if (!slave_wb_we) begin
							// Read requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								0: mmio_cfg_space_tx_buffer = device_id[63:32];
								4: mmio_cfg_space_tx_buffer = device_id[31:0];
								8: mmio_cfg_space_tx_buffer = device_version;
								12: mmio_cfg_space_tx_buffer = sid_adr;
								16: mmio_cfg_space_tx_buffer = control_reg;
								20: mmio_cfg_space_tx_buffer = status_reg;
								24: mmio_cfg_space_tx_buffer = tx_data;
								28: mmio_cfg_space_tx_buffer = rx_data;
								32: mmio_cfg_space_tx_buffer = dma_irq_reg;
								default: mmio_cfg_space_tx_buffer = 0;
							endcase

							// Endian swap
							slave_wb_dat_r_reg[31:24] <= mmio_cfg_space_tx_buffer[7:0];
							slave_wb_dat_r_reg[23:16] <= mmio_cfg_space_tx_buffer[15:8];
							slave_wb_dat_r_reg[15:8] <= mmio_cfg_space_tx_buffer[23:16];
							slave_wb_dat_r_reg[7:0] <= mmio_cfg_space_tx_buffer[31:24];

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end else begin
							// Write requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								// Device ID / version registers cannot be written, don't even try...
								12: mmio_cfg_space_rx_buffer = sid_adr;
								16: mmio_cfg_space_rx_buffer = control_reg;
								24: mmio_cfg_space_rx_buffer = tx_data;
								// Status registers cannot be written, don't even try...
								default: mmio_cfg_space_rx_buffer = 0;
							endcase

							if (slave_wb_sel[0]) begin
								mmio_cfg_space_rx_buffer[7:0] = slave_wb_dat_w[31:24];
							end
							if (slave_wb_sel[1]) begin
								mmio_cfg_space_rx_buffer[15:8] = slave_wb_dat_w[23:16];
							end
							if (slave_wb_sel[2]) begin
								mmio_cfg_space_rx_buffer[23:16] = slave_wb_dat_w[15:8];
							end
							if (slave_wb_sel[3]) begin
								mmio_cfg_space_rx_buffer[31:24] = slave_wb_dat_w[7:0];
							end

							case ({mmio_buffer_address_reg[7:2], 2'b00})
								12: sid_adr <= mmio_cfg_space_rx_buffer;
								16: control_reg <= mmio_cfg_space_rx_buffer;
								24: tx_data <= mmio_cfg_space_rx_buffer;
							endcase

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end
					end
				end
				MMIO_TRANSFER_STATE_TR01: begin
					// Cycle complete
					slave_wb_ack_reg <= 0;
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
				default: begin
					// Should never reach this state
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
			endcase
		end
	end

	// FSI bus signals
	wire fsi_clock_internal;
	reg fsi_data_in_reg = 0;
	wire fsi_data_out_internal;
	reg fsi_data_out_reg = 0;
	wire fsi_data_direction_internal;
	reg fsi_data_direction_reg = 0;
	always @(posedge fsi_clock_internal) begin
		fsi_data_out_reg <= fsi_data_out_internal;
		fsi_data_direction_reg <= fsi_data_direction_internal;
		fsi_data_in_reg <= fsi_data_in;
	end
	assign fsi_data_out = fsi_data_out_reg;
	assign fsi_data_direction = fsi_data_direction_reg;
	assign fsi_clock_out = ~fsi_clock_internal;

	// Generate 1.5MHz FSI clock
	// NOTE: This could be configured to run up to 166MHz according to the specification
	reg [3:0] fsi_clock_divider = 0;
	wire fsi_master_core_clock;
	always @(posedge peripheral_clock) begin
		fsi_clock_divider <= fsi_clock_divider + 1;
	end
	assign fsi_master_core_clock = fsi_clock_divider[3];

	wire [1:0] slave_id;
	wire [20:0] address;
	wire [1:0] data_length;
	wire data_direction;
	wire enable_irq;
	wire enable_relative_address;
	wire enable_extended_address_mode;
	wire enable_crc_protection;
	wire start_cycle;
	wire cycle_complete;
	wire [2:0] cycle_error;
	wire [1:0] enhanced_error_recovery;
	wire [7:0] internal_cmd_issue_delay;
	wire [3:0] ipoll_enable_slave_id;
	wire enable_ipoll;
	wire ipoll_error;
	wire [7:0] interrupt_field;
	wire [11:0] dma_control_field;
	wire [7:0] debug_port;

	fsi_master_interface U1(
		.slave_id(slave_id),
		.address(address),
		.tx_data(tx_data),
		.rx_data(rx_data),
		.data_length(data_length),
		.data_direction(data_direction),
		.enable_relative_address(enable_relative_address),
		.enable_extended_address_mode(enable_extended_address_mode),
		.enable_crc_protection(enable_crc_protection),
		.start_cycle(start_cycle),
		.cycle_complete(cycle_complete),
		.cycle_error(cycle_error),
		.enhanced_error_recovery(enhanced_error_recovery),
		.internal_cmd_issue_delay(internal_cmd_issue_delay),
		.ipoll_enable_slave_id(ipoll_enable_slave_id),
		.enable_ipoll(enable_ipoll),
		.ipoll_error(ipoll_error),
		.interrupt_field(interrupt_field),
		.dma_control_field(dma_control_field),

		.debug_port(debug_port),

		.fsi_data_out(fsi_data_out_internal),
		.fsi_data_in(fsi_data_in_reg),
		.fsi_data_direction(fsi_data_direction_internal),
		.fsi_clock_out(fsi_clock_internal),

		.peripheral_reset(peripheral_reset),
		.peripheral_clock(fsi_master_core_clock)
	);

	// Map individual signals to external registers
	assign slave_id = sid_adr[30:29];
	assign address = sid_adr[28:8];
	assign data_length = sid_adr[1:0];
	assign data_direction = control_reg[1];
	assign enable_irq = control_reg[21];
	assign enable_relative_address = control_reg[20];
	assign enable_extended_address_mode = control_reg[19];
	assign enable_crc_protection = control_reg[18];
	assign start_cycle = control_reg[0];
	assign enhanced_error_recovery = control_reg[17:16];
	assign internal_cmd_issue_delay = control_reg[15:8];
	assign ipoll_enable_slave_id = control_reg[31:28];
	assign enable_ipoll = control_reg[24];
	assign status_reg = {debug_port, 8'b0, ipoll_error, 4'b0, cycle_error, 7'b0, cycle_complete};
	assign dma_irq_reg = {4'b0, dma_control_field, 8'b0, interrupt_field};

	// Handle IRQ assertions
	always @(posedge peripheral_clock) begin
		if (enable_irq) begin
			irq_o = (interrupt_field != 0);
		end else begin
			irq_o = 1'b0;
		end
	end
endmodule

module fsi_master_interface(
		input wire [1:0] slave_id,
		input wire [20:0] address,
		input wire [31:0] tx_data,
		output reg [31:0] rx_data,
		input wire [1:0] data_length,			// 0 == 8 bit, 1 == 16 bit, 2 = 32 bit (NOTE: the lower two address bits may be forced if 16 bit / 32 bit transfer length is set)
		input wire data_direction,			// 0 == read from slave, 1 == write to slave
		input wire enable_relative_address,
		input wire enable_extended_address_mode,	// In 23-bit extended address mode, the complete address is {slave_id, address}
		input wire enable_crc_protection,
		input wire start_cycle,
		output wire cycle_complete,
		output wire [2:0] cycle_error,
		input wire [1:0] enhanced_error_recovery,	// Bit 1 == EER for IPOLL, Bit 0 == EER for all other transmissions
		input wire [7:0] internal_cmd_issue_delay,
		input wire [3:0] ipoll_enable_slave_id,		// Bitwise field, enable for I_POLL to slaves 3 - 0
		input wire enable_ipoll,
		output wire ipoll_error,
		output wire [7:0] interrupt_field,
		output wire [11:0] dma_control_field,

		output wire fsi_data_out,			// Must have I/O output register enabled in top level SB_IO or equivalent, output data driven at rising edge of clock
		input wire fsi_data_in,
		output wire fsi_data_direction,			// 0 == tristate (input), 1 == driven (output)
		output wire fsi_clock_out,			// Must be inverted at the edge driver -- rising clocks are in reference to this signal, not the electrically inverted signal on the FSI bus

		output wire [7:0] debug_port,

		input wire peripheral_reset,
		input wire peripheral_clock
	);

	parameter FSI_TIMEOUT_CYCLES = 256;
	parameter FSI_TURNAROUND_CYCLES = 3;
	parameter FSI_MASTER_TIMEOUT_CYCLES = 166000;	// Assumes 166MHz clock, 1ms timeout
	parameter FSI_IPOLL_IDLE_START_CYCLES = 128;	// This fires every ~786ns during idle periods if a 166MHz clock is used
	parameter FSI_IPOLL_MAX_SEQ_STD_COMMANDS = 2;	// Two commands can fire back-to-back without an IPOLL before an IPOLL is forcibly inserted

	// Command codes are variable length
	parameter FSI_CODEWORD_TX_MSG_BREAK_LEN = 256;
	parameter FSI_CODEWORD_TX_MSG_ABS_ADR_DAT = 3'b100;
	parameter FSI_CODEWORD_TX_MSG_ABS_ADR_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_REL_ADR_DAT = 3'b101;
	parameter FSI_CODEWORD_TX_MSG_REL_ADR_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_SAME_ADR_DAT = 2'b11;
	parameter FSI_CODEWORD_TX_MSG_SAME_ADR_LEN = 2;
	parameter FSI_CODEWORD_TX_MSG_D_POLL_DAT = 3'b010;
	parameter FSI_CODEWORD_TX_MSG_D_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_E_POLL_DAT = 3'b011;
	parameter FSI_CODEWORD_TX_MSG_E_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_I_POLL_DAT = 3'b001;
	parameter FSI_CODEWORD_TX_MSG_I_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_TERM_DAT = 6'b111111;
	parameter FSI_CODEWORD_TX_MSG_TERM_LEN = 6;

	// All response codes are 2 bits long
	parameter FSI_CODEWORD_RX_MSG_ACK = 2'b00;
	parameter FSI_CODEWORD_RX_MSG_BUSY = 2'b01;
	parameter FSI_CODEWORD_RX_MSG_ERR_A = 2'b10;
	parameter FSI_CODEWORD_RX_MSG_ERR_C = 2'b11;

	parameter FSI_ERROR_NONE = 0;
	parameter FSI_ERROR_TIMEOUT = 1;
	parameter FSI_ERROR_MASTER_TIMEOUT = 2;
	parameter FSI_ERROR_INVALID_CODEWORD = 3;
	parameter FSI_ERROR_BAD_TX_CHECKSUM = 4;
	parameter FSI_ERROR_BAD_RX_CHECKSUM = 5;
	parameter FSI_ERROR_INTERNAL_FAULT = 6;

	parameter FSI_INITIALIZE_STATE_01 = 0;
	parameter FSI_INITIALIZE_STATE_02 = 1;
	parameter FSI_INITIALIZE_STATE_03 = 2;
	parameter FSI_TRANSFER_STATE_IDLE = 16;
	parameter FSI_TRANSFER_STATE_TX01 = 17;
	parameter FSI_TRANSFER_STATE_TX02 = 18;
	parameter FSI_TRANSFER_STATE_TX03 = 19;
	parameter FSI_TRANSFER_STATE_TX04 = 20;
	parameter FSI_TRANSFER_STATE_TX05 = 21;
	parameter FSI_TRANSFER_STATE_TX06 = 22;
	parameter FSI_TRANSFER_STATE_TX07 = 23;
	parameter FSI_TRANSFER_STATE_TX08 = 24;
	parameter FSI_TRANSFER_STATE_TX09 = 25;
	parameter FSI_TRANSFER_STATE_TX10 = 26;
	parameter FSI_TRANSFER_STATE_TX11 = 27;
	parameter FSI_TRANSFER_STATE_RX01 = 32;
	parameter FSI_TRANSFER_STATE_RX02 = 33;
	parameter FSI_TRANSFER_STATE_RX03 = 34;
	parameter FSI_TRANSFER_STATE_RX04 = 35;
	parameter FSI_TRANSFER_STATE_RX05 = 36;
	parameter FSI_TRANSFER_STATE_RX06 = 37;
	parameter FSI_TRANSFER_STATE_RX07 = 38;
	parameter FSI_TRANSFER_STATE_RX08 = 39;
	parameter FSI_TRANSFER_STATE_IR01 = 48;
	parameter FSI_TRANSFER_STATE_IR02 = 49;
	parameter FSI_TRANSFER_STATE_IR03 = 50;
	parameter FSI_TRANSFER_STATE_IR04 = 51;
	parameter FSI_TRANSFER_STATE_IR05 = 52;
	parameter FSI_TRANSFER_STATE_TR01 = 64;
	parameter FSI_TRANSFER_STATE_TR02 = 65;
	parameter FSI_TRANSFER_STATE_DL01 = 96;

	reg fsi_data_reg = 0;
	wire fsi_data_in_internal;
	reg fsi_data_direction_reg = 0;
	reg cycle_complete_reg = 0;
	reg [2:0] cycle_error_reg = 0;
	reg ipoll_error_reg = 0;
	reg [7:0] interrupt_field_reg = 0;
	reg [11:0] dma_control_field_reg = 0;
	assign fsi_clock_out = peripheral_clock;	// Clock is allowed to always run
	assign fsi_data_out = ~fsi_data_reg;		// FSI data line is electrically inverted
	assign fsi_data_in_internal = ~fsi_data_in;
	assign fsi_data_direction = fsi_data_direction_reg;
	assign cycle_complete = cycle_complete_reg;
	assign cycle_error = cycle_error_reg;
	assign ipoll_error = ipoll_error_reg;
	assign interrupt_field = interrupt_field_reg;
	assign dma_control_field = dma_control_field_reg;

	// Low level protocol handler
	reg data_direction_reg = 0;
	reg [1:0] data_length_reg = 0;
	reg [1:0] slave_id_reg = 0;
	reg [20:0] address_reg = 0;
	reg [20:0] address_tx_reg = 0;
	reg [31:0] tx_data_reg = 0;
	reg [31:0] rx_data_reg = 0;
	reg enable_relative_address_reg = 0;
	reg enable_extended_address_mode_reg = 0;
	reg enable_crc_protection_reg = 0;
	reg [1:0] enhanced_error_recovery_reg = 0;
	reg [7:0] internal_cmd_issue_delay_reg = 0;
	reg [8:0] cycle_counter = 0;
	reg [7:0] delay_counter = 0;
	reg [20:0] last_address = 0;
	reg last_address_valid = 0;
	reg [7:0] fsi_command_code = 0;
	reg [7:0] fsi_command_code_length = 0;
	reg fsi_command_code_set = 0;
	reg crc_protected_bits_transmitting = 0;
	reg crc_protected_bits_receiving = 0;
	reg fsi_data_reg_internal = 0;
	reg [3:0] crc_data = 0;
	reg crc_feedback = 0;
	reg [7:0] control_state = 0;
	reg [7:0] post_delay_control_state = 0;
	reg [1:0] rx_slave_id = 0;
	reg [1:0] rx_message_type = 0;
	reg [8:0] timeout_counter = 0;
	reg fsi_rel_address_delta_negative = 0;
	reg [7:0] fsi_rel_address_delta = 0;
	reg fsi_master_timeout_counting = 0;
	reg [23:0] fsi_master_timeout_counter = 0;
	reg [1:0] slave_error_recovery_state = 0;
	reg [1:0] master_error_recovery_state = 0;
	reg [7:0] ipoll_start_timer = 0;
	reg enable_ipoll_reg = 0;
	reg [3:0] ipoll_enable_slave_id_reg = 0;
	reg [1:0] ipoll_slave_id = 0;
	reg ipoll_in_process = 0;
	reg busy_response_in_process = 0;
	reg [1:0] commands_since_last_ipoll = 0;
	reg [7:0] interrupt_field_internal = 0;
	reg [11:0] dma_control_field_internal = 0;

	assign debug_port = control_state;

	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			cycle_complete_reg <= 0;
			rx_data <= 0;
			last_address_valid <= 0;
			ipoll_in_process <= 0;
			busy_response_in_process <= 0;
			control_state <= FSI_INITIALIZE_STATE_01;
		end else begin
			case (control_state)
				FSI_INITIALIZE_STATE_01: begin
					// Global (re)-initialization
					ipoll_in_process <= 0;
					busy_response_in_process <= 0;

					// Set up BREAK command
					cycle_counter <= FSI_CODEWORD_TX_MSG_BREAK_LEN;
					control_state <= FSI_INITIALIZE_STATE_02;
				end
				FSI_INITIALIZE_STATE_02: begin
					// Send BREAK command
					// BREAK is *not* CRC protected!
					if (cycle_counter > 0) begin
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 0;
						crc_protected_bits_receiving = 0;
						fsi_data_reg_internal = 1;
						if (cycle_counter == 1) begin
							cycle_counter <= 0;
							control_state <= FSI_INITIALIZE_STATE_03;
						end else begin
							cycle_counter <= cycle_counter - 1;
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_INITIALIZE_STATE_03: begin
					// Transmission complete, switch direction
					fsi_data_reg_internal = 0;

					if (cycle_counter >= (FSI_TURNAROUND_CYCLES - 1)) begin
						timeout_counter <= 0;
						ipoll_start_timer <= 0;
						fsi_data_direction_reg <= 0;
						if (internal_cmd_issue_delay_reg > 0) begin
							delay_counter <= 1;
							post_delay_control_state <= FSI_TRANSFER_STATE_IDLE;
							control_state <= FSI_TRANSFER_STATE_DL01;
						end else begin
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end else begin
						cycle_counter <= cycle_counter + 1;
					end
				end
				FSI_TRANSFER_STATE_IDLE: begin
					if (busy_response_in_process
						&& ((enable_ipoll_reg && (commands_since_last_ipoll < FSI_IPOLL_MAX_SEQ_STD_COMMANDS)) || !enable_ipoll_reg)) begin
						commands_since_last_ipoll <= commands_since_last_ipoll + 1;
						ipoll_in_process <= 0;
						crc_data <= 0;
						control_state <= FSI_TRANSFER_STATE_TX01;
					end else if (start_cycle
						&& ((enable_ipoll_reg && (commands_since_last_ipoll < FSI_IPOLL_MAX_SEQ_STD_COMMANDS)) || !enable_ipoll_reg)) begin
						data_direction_reg <= data_direction;
						data_length_reg <= data_length;
						slave_id_reg <= slave_id;
						case (data_length_reg)
							0: address_reg <= address[20:0];
							1: address_reg <= {address[20:1], 1'b0};
							2: address_reg <= {address[20:2], 2'b01};
							default: address_reg <= address[20:0];
						endcase
						tx_data_reg <= tx_data;
						enable_relative_address_reg <= enable_relative_address;
						enable_extended_address_mode_reg <= enable_extended_address_mode;
						enable_crc_protection_reg <= enable_crc_protection;
						enhanced_error_recovery_reg <= enhanced_error_recovery;
						slave_error_recovery_state <= 0;
						master_error_recovery_state <= 0;
						fsi_command_code_set <= 0;
						ipoll_in_process <= 0;
						crc_data <= 0;
						cycle_error_reg <= FSI_ERROR_NONE;
						fsi_master_timeout_counting <= 0;
						commands_since_last_ipoll <= commands_since_last_ipoll + 1;
						control_state <= FSI_TRANSFER_STATE_TX01;
					end else if (enable_ipoll_reg && ((ipoll_start_timer > FSI_IPOLL_IDLE_START_CYCLES)
						|| (commands_since_last_ipoll >= FSI_IPOLL_MAX_SEQ_STD_COMMANDS))) begin
						// Send I_POLL
						//
						// NOTE: IPOLL requests can override any other incoming command requests.
						// if no IPOLL has been issued for the past FSI_IPOLL_MAX_SEQ_STD_COMMANDS
						// number of command requests.  This is to ensure that interrupts are not
						// lost under heavy I/O load.
						//
						// We cycle through all enabled slaves, with the slave changing on each I_POLL call.
						// The specification is unfortunately silent as to whether this is the intended
						// mechanism to gather data from all slaves on a bus, but it seems to be a
						// reasonable way to handle the interrupt polling requirements for all slaves...
						if (enable_ipoll_reg && ipoll_enable_slave_id_reg) begin
							ipoll_slave_id = ipoll_slave_id + 1;
							if (!ipoll_enable_slave_id_reg[ipoll_slave_id]) begin
								ipoll_slave_id = ipoll_slave_id + 1;
							end
							if (!ipoll_enable_slave_id_reg[ipoll_slave_id]) begin
								ipoll_slave_id = ipoll_slave_id + 1;
							end
							if (!ipoll_enable_slave_id_reg[ipoll_slave_id]) begin
								ipoll_slave_id = ipoll_slave_id + 1;
							end
							fsi_command_code <= FSI_CODEWORD_TX_MSG_I_POLL_DAT;
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_I_POLL_LEN;
							cycle_counter <= FSI_CODEWORD_TX_MSG_I_POLL_LEN;
							fsi_command_code_set <= 1;
							ipoll_in_process <= 1;
							crc_data <= 0;
							control_state <= FSI_TRANSFER_STATE_TX01;
						end
						commands_since_last_ipoll <= 0;
						ipoll_start_timer <= 0;
					end else begin
						ipoll_in_process <= 0;
						ipoll_start_timer <= ipoll_start_timer + 1;
					end
					if (!enable_ipoll_reg) begin
						ipoll_error_reg <= 0;
					end
					enable_ipoll_reg <= enable_ipoll;
					ipoll_enable_slave_id_reg <= ipoll_enable_slave_id;
					internal_cmd_issue_delay_reg <= internal_cmd_issue_delay;
					cycle_complete_reg <= 0;
				end
				FSI_TRANSFER_STATE_TX01: begin
					// Send start bit
					fsi_data_direction_reg <= 1;
					crc_protected_bits_transmitting = 1;
					fsi_data_reg_internal = 1;
					control_state <= FSI_TRANSFER_STATE_TX02;
				end
				FSI_TRANSFER_STATE_TX02: begin
					// Send slave ID bit 1
					fsi_data_direction_reg <= 1;
					crc_protected_bits_transmitting = 1;
					if (ipoll_in_process) begin
						fsi_data_reg_internal = ipoll_slave_id[1];
					end else begin
						fsi_data_reg_internal = slave_id_reg[1];
					end
					control_state <= FSI_TRANSFER_STATE_TX03;
				end
				FSI_TRANSFER_STATE_TX03: begin
					// Send slave ID bit 2
					fsi_data_direction_reg <= 1;
					crc_protected_bits_transmitting = 1;
					if (ipoll_in_process) begin
						fsi_data_reg_internal = ipoll_slave_id[0];
					end else begin
						fsi_data_reg_internal = slave_id_reg[0];
					end
					if (!fsi_command_code_set) begin
						if (enable_relative_address_reg) begin
							if (last_address_valid && (last_address[20:2] == address_reg[20:2])) begin
								fsi_command_code <= FSI_CODEWORD_TX_MSG_SAME_ADR_DAT;
								fsi_command_code_length <= FSI_CODEWORD_TX_MSG_SAME_ADR_LEN;
								cycle_counter <= FSI_CODEWORD_TX_MSG_SAME_ADR_LEN;
							end else if (last_address_valid && (last_address[20:8] == address_reg[20:8])) begin
								fsi_command_code <= FSI_CODEWORD_TX_MSG_REL_ADR_DAT;
								fsi_command_code_length <= FSI_CODEWORD_TX_MSG_REL_ADR_LEN;
								cycle_counter <= FSI_CODEWORD_TX_MSG_REL_ADR_LEN;
								if (address_reg < last_address) begin
									fsi_rel_address_delta_negative <= 1;
								end else begin
									fsi_rel_address_delta_negative <= 0;
								end
								// Relative delta is actually 9-bit two's complement, but fsi_rel_address_delta_negative is used as bit 8
								fsi_rel_address_delta = address_reg - last_address;
							end else begin
								fsi_command_code <= FSI_CODEWORD_TX_MSG_ABS_ADR_DAT;
								fsi_command_code_length <= FSI_CODEWORD_TX_MSG_ABS_ADR_LEN;
								cycle_counter <= FSI_CODEWORD_TX_MSG_ABS_ADR_LEN;
							end
						end else begin
							fsi_command_code <= FSI_CODEWORD_TX_MSG_ABS_ADR_DAT;
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_ABS_ADR_LEN;
							cycle_counter <= FSI_CODEWORD_TX_MSG_ABS_ADR_LEN;
						end
						fsi_command_code_set <= 1;
					end
					control_state <= FSI_TRANSFER_STATE_TX04;
				end
				FSI_TRANSFER_STATE_TX04: begin
					// Send command code
					if (cycle_counter > 0) begin
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = fsi_command_code[cycle_counter-1];
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							if (((fsi_command_code == FSI_CODEWORD_TX_MSG_D_POLL_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_D_POLL_LEN))
								|| ((fsi_command_code == FSI_CODEWORD_TX_MSG_E_POLL_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_E_POLL_LEN))
								|| ((fsi_command_code == FSI_CODEWORD_TX_MSG_I_POLL_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_I_POLL_LEN))
								|| ((fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN))) begin
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_TX09;
							end else begin
								control_state <= FSI_TRANSFER_STATE_TX05;
							end
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_TX05: begin
					// Send read/write flag
					fsi_data_direction_reg <= 1;
					crc_protected_bits_transmitting = 1;
					fsi_data_reg_internal = ~data_direction_reg;
					control_state <= FSI_TRANSFER_STATE_TX06;
					if ((fsi_command_code == FSI_CODEWORD_TX_MSG_SAME_ADR_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_SAME_ADR_LEN)) begin
						address_tx_reg <= address_reg;
						cycle_counter <= 2;
					end else if ((fsi_command_code == FSI_CODEWORD_TX_MSG_REL_ADR_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_REL_ADR_LEN)) begin
						// Set up bit 8 of the address as the sign bit per the specification.
						//
						// NOTE: The specification is unclear about the meaning of the sign bit,
						// so assume conventional sign bit semantics (1 == negative).
						address_tx_reg <= {address_reg[20:9], fsi_rel_address_delta_negative, fsi_rel_address_delta[7:0]};
						cycle_counter <= 9;
					end else begin
						address_tx_reg <= address_reg;
						cycle_counter <= 21;
					end
				end
				FSI_TRANSFER_STATE_TX06: begin
					// Send address
					if (cycle_counter > 0) begin
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = address_tx_reg[cycle_counter-1];
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							control_state <= FSI_TRANSFER_STATE_TX07;
						end

						// Force lowest address bits to specification-mandated values if required
						case (data_length_reg)
							0: address_tx_reg <= address_tx_reg[20:0];
							1: address_tx_reg <= {address_tx_reg[20:1], 1'b0};
							2: address_tx_reg <= {address_tx_reg[20:2], 2'b01};
							default: address_tx_reg <= address_tx_reg[20:0];
						endcase
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_TX07: begin
					// Send data size bit
					fsi_data_direction_reg <= 1;
					crc_protected_bits_transmitting = 1;
					case (data_length_reg)
						0: fsi_data_reg_internal = 0;
						1: fsi_data_reg_internal = 1;
						2: fsi_data_reg_internal = 1;
						default: fsi_data_reg_internal = 0;
					endcase
					if (data_direction_reg) begin
						// Write
						case (data_length_reg)
							0: begin
								// Byte transfer
								cycle_counter <= 8;
							end
							1: begin
								// Half word transfer
								cycle_counter <= 16;
							end
							2: begin
								// Word transfer
								cycle_counter <= 32;
							end
							default: begin
								// Invalid sizes are treated as byte transfers
								cycle_counter <= 8;
							end
						endcase
						control_state <= FSI_TRANSFER_STATE_TX08;
					end else begin
						// Read
						cycle_counter <= 4;
						control_state <= FSI_TRANSFER_STATE_TX09;
					end
				end
				FSI_TRANSFER_STATE_TX08: begin
					// Send data
					if (cycle_counter > 0) begin
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = tx_data_reg[cycle_counter-1];
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							cycle_counter <= 4;
							control_state <= FSI_TRANSFER_STATE_TX09;
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_TX09: begin
					// Send CRC
					if (cycle_counter > 0) begin
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 0;
						fsi_data_reg_internal = crc_data[cycle_counter-1];
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							control_state <= FSI_TRANSFER_STATE_TX10;
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_TX10: begin
					// Transmission complete, switch direction
					fsi_data_reg_internal = 0;

					if (cycle_counter >= (FSI_TURNAROUND_CYCLES - 1)) begin
						timeout_counter <= 0;
						rx_data_reg <= 0;
						crc_data <= 0;
						fsi_data_direction_reg <= 0;
						control_state <= FSI_TRANSFER_STATE_TX11;
					end else begin
						cycle_counter <= cycle_counter + 1;
					end
				end
				FSI_TRANSFER_STATE_TX11: begin
					// Wait for start signal
					if (fsi_data_in_internal) begin
						crc_protected_bits_receiving = 1;
						if (enable_extended_address_mode_reg) begin
							control_state <= FSI_TRANSFER_STATE_RX03;
						end else begin
							control_state <= FSI_TRANSFER_STATE_RX01;
						end
					end else begin
						if (timeout_counter >= (FSI_TIMEOUT_CYCLES - 1)) begin
							if (ipoll_in_process) begin
								ipoll_error_reg <= 1;
							end else begin
								cycle_error_reg <= FSI_ERROR_TIMEOUT;
							end
							fsi_master_timeout_counting <= 0;
							control_state <= FSI_TRANSFER_STATE_TR01;
						end else begin
							timeout_counter <= timeout_counter + 1;
						end
					end
				end
				FSI_TRANSFER_STATE_RX01: begin
					// Receive slave ID, bit 1
					crc_protected_bits_receiving = 1;
					rx_slave_id[1] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_RX02;
				end
				FSI_TRANSFER_STATE_RX02: begin
					// Receive slave ID, bit 2
					crc_protected_bits_receiving = 1;
					rx_slave_id[0] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_RX03;
				end
				FSI_TRANSFER_STATE_RX03: begin
					// Receive message type, bit 1
					crc_protected_bits_receiving = 1;
					if (ipoll_in_process) begin
						if (fsi_data_in_internal) begin
							// IPOLL response has one bit reserved for type
							// If that bit is not zero, the response is not a valid I_POLL_RSP message
							ipoll_error_reg <= 1;
							control_state <= FSI_TRANSFER_STATE_TR01;
						end else begin
							// Potentially valid I_POLL_RSP message
							control_state <= FSI_TRANSFER_STATE_IR01;
						end
					end else begin
						rx_message_type[1] <= fsi_data_in_internal;
						control_state <= FSI_TRANSFER_STATE_RX04;
					end
				end
				FSI_TRANSFER_STATE_RX04: begin
					// Receive message type, bit 2
					crc_protected_bits_receiving = 1;
					rx_message_type[0] <= fsi_data_in_internal;
					case ({rx_message_type[1], fsi_data_in_internal})
						FSI_CODEWORD_RX_MSG_ACK: begin
							if ((fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN)) begin
								// TERM command sent -- parse ACK message
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_RX06;
							end else begin
								if (data_direction_reg) begin
									// Write -- ACK message
									cycle_counter <= 4;
									control_state <= FSI_TRANSFER_STATE_RX06;
								end else begin
									// Read -- ACK_D message
									case (data_length_reg)
										0: begin
											// Byte transfer
											cycle_counter <= 8;
										end
										1: begin
											// Half word transfer
											cycle_counter <= 16;
										end
										2: begin
											// Word transfer
											cycle_counter <= 32;
										end
										default: begin
											// Invalid sizes are treated as byte transfers
											cycle_counter <= 8;
										end
									endcase
									slave_error_recovery_state <= 0;
									control_state <= FSI_TRANSFER_STATE_RX05;
								end
							end
						end
						FSI_CODEWORD_RX_MSG_BUSY: begin
							if (ipoll_in_process) begin
								// BUSY not allowed as IPOLL response!
								ipoll_error_reg <= 1;
								control_state <= FSI_TRANSFER_STATE_TR01;
							end else begin
								fsi_master_timeout_counting <= 1;
								slave_error_recovery_state <= 0;
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_RX06;
							end
						end
						FSI_CODEWORD_RX_MSG_ERR_A: begin
							// Slave received corrupted message from master and does not support enhanced error recovery
							if (ipoll_in_process) begin
								ipoll_error_reg <= 1;
							end else begin
								cycle_error_reg <= FSI_ERROR_BAD_TX_CHECKSUM;
							end
							control_state <= FSI_TRANSFER_STATE_TR01;
						end
						FSI_CODEWORD_RX_MSG_ERR_C: begin
							// Slave received corrupted message from master and is requesting enhanced error recovery
							if (enhanced_error_recovery_reg[0] && (slave_error_recovery_state == 0)) begin
								// Configure state machine to resend command message
								slave_error_recovery_state <= 1;
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_RX06;
							end else begin
								// Master does not support enhanced error recovery or error recovery fauled
								if (ipoll_in_process) begin
									ipoll_error_reg <= 1;
								end else begin
									cycle_error_reg <= FSI_ERROR_BAD_TX_CHECKSUM;
								end
								control_state <= FSI_TRANSFER_STATE_TR01;
							end
						end
						default: begin
							// NOTE: This could be the result of data corruption!
							// Unfortunately, since the response messages are variable length, there is no
							// way to know the intended message length and therefore no way to run checksum
							// validation at this early stage...
							slave_error_recovery_state <= 0;
							if (ipoll_in_process) begin
								ipoll_error_reg <= 1;
							end else begin
								cycle_error_reg <= FSI_ERROR_INVALID_CODEWORD;
							end
							control_state <= FSI_TRANSFER_STATE_TR01;
						end
					endcase
				end
				FSI_TRANSFER_STATE_RX05: begin
					// Receive data
					if (cycle_counter > 0) begin
						crc_protected_bits_receiving = 1;
						rx_data_reg[cycle_counter-1] = fsi_data_in_internal;
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							cycle_counter <= 4;
							control_state <= FSI_TRANSFER_STATE_RX06;
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_RX06: begin
					// Receive CRC
					if (cycle_counter > 0) begin
						// Load CRC bits into Galios LFSR for verification
						crc_protected_bits_receiving = 1;
						cycle_counter <= cycle_counter - 1;
						if (cycle_counter == 1) begin
							control_state <= FSI_TRANSFER_STATE_RX07;
						end
					end else begin
						// Should never reach this state
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_INITIALIZE_STATE_01;
					end
				end
				FSI_TRANSFER_STATE_RX07: begin
					crc_protected_bits_receiving = 0;
					if ((crc_data != 0) && enable_crc_protection_reg) begin
						// Master received corrupted message from slave
						if (((enhanced_error_recovery_reg[1] && ipoll_in_process)
							|| (enhanced_error_recovery_reg[0] && !ipoll_in_process))
							&& (master_error_recovery_state == 0)) begin
							// Configure state machine to send E_POLL
							master_error_recovery_state <= 1;
							control_state <= FSI_TRANSFER_STATE_TR01;
						end else begin
							// Master does not support enhanced error recovery or error recovery failed
							if (ipoll_in_process) begin
								ipoll_error_reg <= 1;
							end else begin
								cycle_error_reg <= FSI_ERROR_BAD_RX_CHECKSUM;
							end
							control_state <= FSI_TRANSFER_STATE_TR01;
						end
					end else begin
						if (!enable_extended_address_mode_reg && (rx_slave_id != slave_id_reg)) begin
							// Slave message was not meant for this master
							// Keep listening, but do not reset timeout counter here!
							rx_data_reg <= 0;
							crc_data <= 0;
							control_state <= FSI_TRANSFER_STATE_TX11;
						end else begin
							if (ipoll_in_process) begin
								interrupt_field_reg <= interrupt_field_internal;
								dma_control_field_reg <= dma_control_field_internal;
							end
							master_error_recovery_state <= 0;
							control_state <= FSI_TRANSFER_STATE_TR01;
						end
					end
				end
				FSI_TRANSFER_STATE_TR01: begin
					// Reception complete, switch direction
					fsi_data_reg_internal = 0;

					// Stop CRC generation
					crc_protected_bits_transmitting = 0;
					crc_protected_bits_receiving = 0;

					if (cycle_counter >= (FSI_TURNAROUND_CYCLES - 1)) begin
						if (slave_error_recovery_state == 1) begin
							// Resend command
							fsi_command_code_set <= 0;
							crc_data <= 0;
							slave_error_recovery_state <= 2;
							if (internal_cmd_issue_delay_reg > 0) begin
								delay_counter <= 1;
								post_delay_control_state <= FSI_TRANSFER_STATE_TX01;
								control_state <= FSI_TRANSFER_STATE_DL01;
							end else begin
								control_state <= FSI_TRANSFER_STATE_TX01;
							end
						end else if (master_error_recovery_state == 1) begin
							// Send E_POLL
							fsi_command_code <= FSI_CODEWORD_TX_MSG_E_POLL_DAT;
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_E_POLL_LEN;
							cycle_counter <= FSI_CODEWORD_TX_MSG_E_POLL_LEN;
							fsi_command_code_set <= 1;
							crc_data <= 0;
							master_error_recovery_state <= 2;
							if (internal_cmd_issue_delay_reg > 0) begin
								delay_counter <= 1;
								post_delay_control_state <= FSI_TRANSFER_STATE_TX01;
								control_state <= FSI_TRANSFER_STATE_DL01;
							end else begin
								control_state <= FSI_TRANSFER_STATE_TX01;
							end
						end else if (cycle_error_reg == FSI_ERROR_NONE) begin
							if (rx_message_type == FSI_CODEWORD_RX_MSG_BUSY) begin
								if (fsi_master_timeout_counter < FSI_MASTER_TIMEOUT_CYCLES) begin
									// Send D_POLL
									fsi_command_code <= FSI_CODEWORD_TX_MSG_D_POLL_DAT;
									fsi_command_code_length <= FSI_CODEWORD_TX_MSG_D_POLL_LEN;
									cycle_counter <= FSI_CODEWORD_TX_MSG_D_POLL_LEN;
									fsi_command_code_set <= 1;
									busy_response_in_process <= 1;
									crc_data <= 0;
									if (internal_cmd_issue_delay_reg > 0) begin
										delay_counter <= 1;
										post_delay_control_state <= FSI_TRANSFER_STATE_IDLE;
										control_state <= FSI_TRANSFER_STATE_DL01;
									end else begin
										control_state <= FSI_TRANSFER_STATE_IDLE;
									end
								end else begin
									// Send TERM
									fsi_command_code <= FSI_CODEWORD_TX_MSG_TERM_DAT;
									fsi_command_code_length <= FSI_CODEWORD_TX_MSG_TERM_LEN;
									cycle_counter <= FSI_CODEWORD_TX_MSG_TERM_LEN;
									if (ipoll_in_process) begin
										ipoll_error_reg <= 1;
									end else begin
										cycle_error_reg <= FSI_ERROR_MASTER_TIMEOUT;
									end
									fsi_master_timeout_counting <= 0;
									fsi_command_code_set <= 1;
									busy_response_in_process <= 0;
									crc_data <= 0;
									if (internal_cmd_issue_delay_reg > 0) begin
										delay_counter <= 1;
										post_delay_control_state <= FSI_TRANSFER_STATE_TX01;
										control_state <= FSI_TRANSFER_STATE_DL01;
									end else begin
										control_state <= FSI_TRANSFER_STATE_TX01;
									end
								end
							end else begin
								// Transfer complete!
								if (!ipoll_in_process) begin
									rx_data <= rx_data_reg;
									cycle_complete_reg <= 1;
								end
								busy_response_in_process <= 0;
								fsi_master_timeout_counting <= 0;
								control_state <= FSI_TRANSFER_STATE_TR02;
							end
							slave_error_recovery_state <= 0;
							master_error_recovery_state <= 0;
						end else begin
							// Transfer failed!
							if (!ipoll_in_process) begin
								cycle_complete_reg <= 1;
							end
							busy_response_in_process <= 0;
							fsi_master_timeout_counting <= 0;
							control_state <= FSI_TRANSFER_STATE_TR02;
							slave_error_recovery_state <= 0;
							master_error_recovery_state <= 0;
						end
						fsi_data_direction_reg <= 0;
					end else begin
						cycle_counter <= cycle_counter + 1;
					end
				end
				FSI_TRANSFER_STATE_TR02: begin
					if (!start_cycle || ipoll_in_process) begin
						if (cycle_error_reg == FSI_ERROR_NONE) begin
							last_address <= address_tx_reg;
							last_address_valid <= 1;
						end else begin
							last_address_valid <= 0;
						end
						ipoll_start_timer <= 0;
						ipoll_in_process <= 0;
						if (internal_cmd_issue_delay_reg > 0) begin
							delay_counter <= 1;
							post_delay_control_state <= FSI_TRANSFER_STATE_IDLE;
							control_state <= FSI_TRANSFER_STATE_DL01;
						end else begin
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end
				end
				FSI_TRANSFER_STATE_IR01: begin
					// Receive interrupt field, bit 1
					crc_protected_bits_receiving = 1;
					interrupt_field_internal[(ipoll_slave_id*2)+1] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_IR02;
				end
				FSI_TRANSFER_STATE_IR02: begin
					// Receive interrupt field, bit 2
					crc_protected_bits_receiving = 1;
					interrupt_field_internal[(ipoll_slave_id*2)+0] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_IR03;
				end
				FSI_TRANSFER_STATE_IR03: begin
					// Receive DMA control field, bit 1
					crc_protected_bits_receiving = 1;
					dma_control_field_internal[(ipoll_slave_id*3)+2] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_IR04;
				end
				FSI_TRANSFER_STATE_IR04: begin
					// Receive DMA control field, bit 2
					crc_protected_bits_receiving = 1;
					dma_control_field_internal[(ipoll_slave_id*3)+1] <= fsi_data_in_internal;
					control_state <= FSI_TRANSFER_STATE_IR05;
				end
				FSI_TRANSFER_STATE_IR05: begin
					// Receive DMA control field, bit 3
					crc_protected_bits_receiving = 1;
					dma_control_field_internal[(ipoll_slave_id*3)+0] <= fsi_data_in_internal;
					cycle_counter <= 4;
					control_state <= FSI_TRANSFER_STATE_RX06;
				end
				FSI_TRANSFER_STATE_DL01: begin
					if (delay_counter >= internal_cmd_issue_delay_reg) begin
						control_state <= post_delay_control_state;
					end else begin
						delay_counter <= delay_counter + 1;
					end
				end
				default: begin
					// Should never reach this state
					cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
					control_state <= FSI_INITIALIZE_STATE_01;
				end
			endcase
		end

		// CRC calculation
		// Implement Galios-type LFSR for polynomial 0x7 (MSB first)
		if (crc_protected_bits_transmitting) begin
			crc_feedback = crc_data[3] ^ fsi_data_reg_internal;
		end
		if (crc_protected_bits_receiving) begin
			crc_feedback = crc_data[3] ^ fsi_data_in_internal;
		end
		if (crc_protected_bits_transmitting || crc_protected_bits_receiving) begin
			crc_data[0] <= crc_feedback;
			crc_data[1] <= crc_data[0] ^ crc_feedback;
			crc_data[2] <= crc_data[1] ^ crc_feedback;
			crc_data[3] <= crc_data[2];
		end

		// Transmit data
		fsi_data_reg <= fsi_data_reg_internal;

		if (fsi_master_timeout_counting) begin
			fsi_master_timeout_counter <= fsi_master_timeout_counter + 1;
		end else begin
			fsi_master_timeout_counter <= 0;
		end
	end
endmodule
