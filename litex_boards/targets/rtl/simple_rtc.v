// © 2020 - 2021 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module simple_rtc_wishbone #(
		parameter BASE_CLOCK_FREQUENCY_KHZ = 1000
	)
	(
		// Wishbone slave port signals
		input wire slave_wb_cyc,
		input wire slave_wb_stb,
		input wire slave_wb_we,
		input wire [29:0] slave_wb_addr,
		input wire [31:0] slave_wb_dat_w,
		output wire [31:0] slave_wb_dat_r,
		input wire [3:0] slave_wb_sel,
		output wire slave_wb_ack,
		output wire slave_wb_err,

		input wire peripheral_reset,
		input wire peripheral_clock,

		input wire [31:0] slave_wb_dat_w,
		input wire rtc_clock
	);

	// Control and status registers
	wire [63:0] device_id;
	wire [31:0] device_version;
	reg [31:0] control_reg1 = 0;
	reg [31:0] control_reg2 = 0;
	wire [31:0] status_reg1;

	// Device identifier
	assign device_id = 64'h7c52505453525443;
	assign device_version = 32'h00010000;

	reg slave_wb_ack_reg = 0;
	reg [31:0] slave_wb_dat_r_reg = 0;

	assign slave_wb_ack = slave_wb_ack_reg;
	assign slave_wb_dat_r = slave_wb_dat_r_reg;

	parameter MMIO_TRANSFER_STATE_IDLE = 0;
	parameter MMIO_TRANSFER_STATE_TR01 = 1;

	reg [31:0] mmio_buffer_address_reg = 0;
	reg [7:0] mmio_transfer_state = 0;
	reg [31:0] mmio_cfg_space_tx_buffer = 0;
	reg [31:0] mmio_cfg_space_rx_buffer = 0;

	// Wishbone connector
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			// Reset Wishbone interface / control state machine
			slave_wb_ack_reg <= 0;

			mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
		end else begin
			case (mmio_transfer_state)
				MMIO_TRANSFER_STATE_IDLE: begin
					// Compute effective address
					mmio_buffer_address_reg[31:2] = slave_wb_addr;
					case (slave_wb_sel)
						4'b0001: mmio_buffer_address_reg[1:0] = 0;
						4'b0010: mmio_buffer_address_reg[1:0] = 1;
						4'b0100: mmio_buffer_address_reg[1:0] = 2;
						4'b1000: mmio_buffer_address_reg[1:0] = 3;
						4'b1111: mmio_buffer_address_reg[1:0] = 0;
						default: mmio_buffer_address_reg[1:0] = 0;
					endcase

					if (slave_wb_cyc && slave_wb_stb) begin
						// Configuration register space access
						// Single clock pulse signals in deasserted state...process incoming request!
						if (!slave_wb_we) begin
							// Read requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								0: mmio_cfg_space_tx_buffer = device_id[63:32];
								4: mmio_cfg_space_tx_buffer = device_id[31:0];
								8: mmio_cfg_space_tx_buffer = device_version;
								12: mmio_cfg_space_tx_buffer = control_reg1;
								16: mmio_cfg_space_tx_buffer = control_reg2;
								20: mmio_cfg_space_tx_buffer = status_reg1;
								default: mmio_cfg_space_tx_buffer = 0;
							endcase

							// Endian swap
							slave_wb_dat_r_reg[31:24] <= mmio_cfg_space_tx_buffer[7:0];
							slave_wb_dat_r_reg[23:16] <= mmio_cfg_space_tx_buffer[15:8];
							slave_wb_dat_r_reg[15:8] <= mmio_cfg_space_tx_buffer[23:16];
							slave_wb_dat_r_reg[7:0] <= mmio_cfg_space_tx_buffer[31:24];

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end else begin
							// Write requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								// Device ID / version registers cannot be written, don't even try...
								12: mmio_cfg_space_rx_buffer = control_reg1;
								16: mmio_cfg_space_rx_buffer = control_reg2;
								// Status registers cannot be written, don't even try...
								default: mmio_cfg_space_rx_buffer = 0;
							endcase

							if (slave_wb_sel[0]) begin
								mmio_cfg_space_rx_buffer[7:0] = slave_wb_dat_w[31:24];
							end
							if (slave_wb_sel[1]) begin
								mmio_cfg_space_rx_buffer[15:8] = slave_wb_dat_w[23:16];
							end
							if (slave_wb_sel[2]) begin
								mmio_cfg_space_rx_buffer[23:16] = slave_wb_dat_w[15:8];
							end
							if (slave_wb_sel[3]) begin
								mmio_cfg_space_rx_buffer[31:24] = slave_wb_dat_w[7:0];
							end

							case ({mmio_buffer_address_reg[7:2], 2'b00})
								12: control_reg1 <= mmio_cfg_space_rx_buffer;
								16: control_reg2 <= mmio_cfg_space_rx_buffer;
							endcase

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end
					end
				end
				MMIO_TRANSFER_STATE_TR01: begin
					// Cycle complete
					slave_wb_ack_reg <= 0;
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
				default: begin
					// Should never reach this state
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
			endcase
		end
	end

	// Instantiate simple RTC module and connect signals
	simple_rtc simple_rtc(
		.base_clock(rtc_clock),
		.clock_set(control_reg2),
		.current_time(status_reg1),
		.set_strobe(control_reg1[0])
	);
endmodule

module simple_rtc #(
		parameter BASE_CLOCK_FREQUENCY_KHZ = 1000
	)
	(
		input wire base_clock,
		input wire [31:0] clock_set,
		output wire [31:0] current_time,
		input wire set_strobe
	);

	reg [31:0] current_time_reg = 1577836800;	// 01/01/2020 00:00:00 UTC
	reg [31:0] seconds_counter = 0;
	reg set_strobe_prev = 0;

	assign current_time = current_time_reg;

	always @(posedge base_clock) begin
		if (set_strobe && !set_strobe_prev) begin
			current_time_reg <= clock_set;
			seconds_counter <= 0;
		end else begin
			if (seconds_counter > (BASE_CLOCK_FREQUENCY_KHZ * 1000)) begin
				current_time_reg <= current_time_reg + 1;
				seconds_counter <= 0;
			end else begin
				seconds_counter <= seconds_counter + 1;
			end
		end

		set_strobe_prev <= set_strobe;
	end
endmodule