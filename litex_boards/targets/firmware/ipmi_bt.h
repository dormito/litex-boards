// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

#ifndef _IPMI_BT_H
#define _IPMI_BT_H

// Bitfield flag shifts
#define IPMI_BT_CTL_CLEAR_WR_PTR_SHIFT	0
#define IPMI_BT_CTL_CLEAR_RD_PTR_SHIFT	1
#define IPMI_BT_CTL_H2B_ATN_SHIFT	2
#define IPMI_BT_CTL_B2H_ATN_SHIFT	3
#define IPMI_BT_CTL_SMS_ATN_SHIFT	4
#define IPMI_BT_CTL_OEM0_SHIFT		5
#define IPMI_BT_CTL_H_BUSY_SHIFT	6
#define IPMI_BT_CTL_B_BUSY_SHIFT	7

// Sensor mappings
// NOTE: These may change from platform to platform!
// The values below are valid for Raptor Computing Systems Talos II / Blackbird
#define IPMI_SENSOR_FW_BOOT		0x02	// XML ID /sys-0/fw_boot_sensor [W]
#define IPMI_SENSOR_CPU0_OCC_ACTIVE	0x03	// XML ID /sys-0/node-0/motherboard-0/proc_socket-0/module-0/p9_proc_s/occ/occ_active_sensor [W]
#define IPMI_SENSOR_CPU1_OCC_ACTIVE	0x04	// XML ID /sys-0/node-0/motherboard-0/proc_socket-1/module-0/p9_proc_s/occ/occ_active_sensor [W]
#define IPMI_SENSOR_CPU0_FUNC		0x08	// XML ID /sys-0/node-0/motherboard-0/proc_socket-0/module-0/p9_proc_s/cpu_func_sensor [W]
#define IPMI_SENSOR_CPU1_FUNC		0x09	// XML ID /sys-0/node-0/motherboard-0/proc_socket-1/module-0/p9_proc_s/cpu_func_sensor [W]
#define IPMI_SENSOR_BOOT_COUNT		0x8b	// XML ID /sys-0/boot_count_sensor [RW]
#define IPMI_SENSOR_PLANAR_FAULT	0x8c	// XML ID /sys-0/node-0/motherboard_fault_sensor [W]
#define IPMI_SENSOR_REF_CLK_FAULT	0x8d	// XML ID /sys-0/node-0/ref_clk_sensor [W]
#define IPMI_SENSOR_PCIE_CLK_FAULT	0x8e	// XML ID /sys-0/node-0/pci_clk_sensor [W]
#define IPMI_SENSOR_TOD_CLK_FAULT	0x8f	// XML ID /sys-0/node-0/tod_clk_sensor [W]
#define IPMI_SENSOR_APSS_FAULT		0x93	// XML ID /sys-0/node-0/apss_fault_sensor [W]
#define IPMI_SENSOR_DERATING_FACTOR	0x96	// XML ID /sys-0/ps_derating_sensor [R]

// Inventory bitfield sensor ranges
#define IPMI_SENSOR_INV_DIMM_FUNC_BEG	0x0b	// XML ID /sys-0/node-0/motherboard-0/dimmconn-<X>/dimm-0/dimm_func_sensor [W]
#define IPMI_SENSOR_INV_DIMM_FUNC_END	0x1a
#define IPMI_SENSOR_INV_CPU0_FUNC_BEG	0x2b	// XML ID /sys-0/node-0/motherboard-0/proc_socket-0/module-0/p9_proc_s/eq<X>/ex<Y>/core<Z>/cpucore_func_sensor [W]
#define IPMI_SENSOR_INV_CPU0_FUNC_END	0x42
#define IPMI_SENSOR_INV_CPU1_FUNC_BEG	0x43	// XML ID /sys-0/node-0/motherboard-0/proc_socket-1/module-0/p9_proc_s/eq<X>/ex<Y>/core<Z>/cpucore_func_sensor [W]
#define IPMI_SENSOR_INV_CPU1_FUNC_END	0x5a

// Event Data Byte bitfield mappings
// These are believed constant across hostboot versions, and are defined in hostboot src/include/usr/ipmi/ipmisensor.H
#define IPMI_OP_EVT_SHIFT_DIMM_DSBL	4
#define IPMI_OP_EVT_SHIFT_DIMM_PRST	6
#define IPMI_OP_EVT_SHIFT_PROC_PRST	7
#define IPMI_OP_EVT_SHIFT_PROC_DSBL	8

// IPMI network function codes
#define IPMI_NETFN_SENS_ET_REQ		0x04
#define IPMI_NETFN_APP_REQUEST		0x06
#define IPMI_NETFN_STORAGE_REQ		0x0a
#define IPMI_NETFN_DCMI_GP_REQ		0x2c
#define IPMI_NETFN_OEM_IBM_REQ		0x3a

// Sensor / Event commands
#define IPMI_CMD_GET_SNS_READNG		0x2d
#define IPMI_CMD_SET_SN_RD_EV_S		0x30

// Application commands
#define IPMI_CMD_GET_DEVICE_ID		0x01
#define IPMI_CMD_GET_BT_INT_CAP		0x36

// Storage commands
#define IPMI_CMD_GET_SEL_TIME		0x48
#define IPMI_CMD_SET_SEL_TIME		0x49

// IBM commands
#define IPMI_CMD_IBM_HIOMAP_REQ		0x5a

#define IPMI_CC_NO_ERROR		0x00
#define IPMI_CC_SNS_NOT_PRSNT		0xcb
#define IPMI_CC_INVALID_COMMAND		0xc1

#define BASE_IPMI_REQUEST_LENGTH	3
#define BASE_IPMI_RESPONSE_LENGTH	4
#define BASE_DCMI_RESPONSE_LENGTH	(BASE_IPMI_RESPONSE_LENGTH + 1)
#define BASE_HIOMAP_RESPONSE_LENGTH	(BASE_IPMI_RESPONSE_LENGTH + 2)

// DCMI commands
#define DCMI_CMD_GET_CAPABILITIES	0x01
#define DCMI_CMD_GET_POWER_CAP		0x03

// DCMI response codes
#define DCMI_CC_NO_ERROR		0x00
#define DCMI_CC_INVALID_COMMAND		0xc1

// DCMI parameters
#define DCMI_ENFORCE_POWER_LIMIT	0
#define DCMI_POWER_LIMIT_W		190
#define DCMI_CORR_LIMIT_TIME_MS		500
#define DCMI_PWR_SAMPLE_PERIOD_S	1

// HIOMAP commands
#define HIOMAP_CMD_RESET		0x01
#define HIOMAP_CMD_GET_INFO		0x02
#define HIOMAP_CMD_GET_FLASH_INFO	0x03
#define HIOMAP_CMD_CREATE_RD_WIN	0x04
#define HIOMAP_CMD_CLOSE_WINDOW		0x05
#define HIOMAP_CMD_CREATE_WR_WIN	0x06
#define HIOMAP_CMD_MARK_DIRTY		0x07
#define HIOMAP_CMD_FLUSH		0x08
#define HIOMAP_CMD_ACK			0x09
#define HIOMAP_CMD_ERASE		0x0a
#define HIOMAP_CMD_GET_FLASH_NAME	0x0b
#define HIOMAP_CMD_LOCK			0x0c

#define HIOMAP_WINDOW_TYPE_READ		0x0
#define HIOMAP_WINDOW_TYPE_WRITE	0x1
#define HIOMAP_WINDOW_INACTIVE 		0x2

// HIOMAP parameters
#define HIOMAP_SUGGESTED_TIMEOUT_S	15
#define HIOMAP_PNOR_DEVICE_COUNT	1

// 4k block size
// Requires subsector erase capability
#define FLASH_BLOCK_SIZE_SHIFT		12
// 64MB Flash
#define FLASH_SIZE_BYTES		67108864
// 256 byte pages
#define FLASH_PAGE_SIZE_BYTES		256
// Minimum erase size is one block
#define FLASH_ERASE_GRAN_BLOCKS		1
// Maximum cacheable / writeable chunk size
// Constrained by available BRAM in the FPGA
#define FLASH_MAX_WR_WINDOW_BYTES	4096

// Calculated and specification locked HIOMAP parameters
#define LPC_ADDRESS_BITS		28
#define FLASH_SIZE_BLOCKS		(FLASH_SIZE_BYTES >> FLASH_BLOCK_SIZE_SHIFT)
#define FLASH_ERASE_GRAN_BYTES		(FLASH_ERASE_GRAN_BLOCKS << FLASH_BLOCK_SIZE_SHIFT)

// OpenPOWER system constants
#define OPENPOWER_BOOT_COUNT_SENSOR_DEFAULT	2

typedef struct hiomap_pnor_dirty_range {
	uint32_t start_address;
	uint32_t bytes;
	uint8_t erased;
} hiomap_pnor_dirty_range_t;

typedef struct hiomap_configuration_data {
	uint8_t protocol_version;
	uint32_t window_start_address;
	uint32_t window_length_bytes;
	uint8_t active_device_id;
	uint8_t window_type;
	uint8_t dirty_range_count;
	hiomap_pnor_dirty_range_t dirty_ranges[128];
} hiomap_configuration_data_t;

typedef struct __attribute__((packed)) ipmi_request_message {
	uint8_t length;
	uint8_t netfn_lun;
	uint8_t sequence;
	uint8_t command;
	uint8_t data[256 - BASE_IPMI_REQUEST_LENGTH];
} ipmi_request_message_t;

typedef struct __attribute__((packed)) ipmi_response_message {
	uint8_t length;
	uint8_t netfn_lun;
	uint8_t sequence;
	uint8_t command;
	uint8_t completion_code;
	uint8_t data[256 - BASE_IPMI_REQUEST_LENGTH];
} ipmi_response_message_t;

#endif // _IPMI_BT_H
