#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2021 Raptor Engineering, LLC
# License: BSD

import os
import argparse

from migen import *
from migen.genlib.resetsync import AsyncResetSynchronizer

from litex_boards.platforms import versa_ecp5

from litex.build.lattice.trellis import trellis_args, trellis_argdict

from litex.soc.cores.clock import *
from litex.soc.integration.soc import SoCRegion
from litex.soc.integration.soc_core import *
from litex.soc.integration.soc_sdram import *
from litex.soc.integration.builder import *
from litex.soc.cores.led import LedChaser

from litedram.modules import MT41K64M16
from litedram.phy import ECP5DDRPHY

from liteeth.phy.ecp5rgmii import LiteEthPHYRGMII

from litespi.modules import N25Q512
from litespi.opcodes import SpiNorFlashOpCodes as Codes
from litespi.phy.generic import LiteSPIPHY
from litespi import LiteSPI

kB = 1024
mB = 1024*kB

# LPC Slave interface ------------------------------------------------------------------------------

from litex.soc.interconnect.csr_eventmanager import EventManager, EventSourceLevel
from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

class AquilaLPCSlave(Module, AutoCSR):
    def __init__(self, platform, pads, endianness="big", adr_offset=0x0, debug_signals=None, lpc_clk_mirror=None):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)
        self.master_bus   = master_bus   = wishbone.Interface(data_width=32, adr_width=30)
        self.wb_irq = Signal()

        # Set up IRQ handling
        self.submodules.ev = EventManager()
        self.ev.master_interrupt = EventSourceLevel(name="IRQ", description="Aquila master interrupt, asserted for any active interrupt condition in the slave core")
        self.ev.finalize()

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.master_dat_w = Signal(32)
        self.master_dat_r = Signal(32)
        self.comb += self.master_dat_r.eq(master_bus.dat_r if endianness == "big" else reverse_bytes(master_bus.dat_r))
        self.comb += master_bus.dat_w.eq(self.master_dat_w if endianness == "big" else reverse_bytes(self.master_dat_w))

        # Calculate slave address
        self.slave_address = Signal(30)
        self.comb += self.slave_address.eq(slave_bus.adr - (adr_offset >> 2)) # wb address is in words, offset is in bytes

        # Debug signals
        self.debug_port = Signal(16)
        self.lpc_clock_mirror = Signal()

        # LPC interface signals
        self.lpc_data_out = Signal(4)
        self.lpc_data_in = Signal(4)
        self.lpc_data_direction = Signal()
        self.lpc_irq_out = Signal()
        self.lpc_irq_in = Signal()
        self.lpc_irq_direction = Signal()
        self.lpc_frame_n = Signal()
        self.lpc_reset_n = Signal()
        self.lpc_clock = Signal()

        self.specials += Instance("aquila_lpc_slave_wishbone",
            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = self.slave_address,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,
            o_slave_irq_o = self.wb_irq,

            # Wishbone master port signals
            o_master_wb_cyc = master_bus.cyc,
            o_master_wb_stb = master_bus.stb,
            o_master_wb_we = master_bus.we,
            o_master_wb_addr = master_bus.adr,
            o_master_wb_dat_w = self.master_dat_w,
            i_master_wb_dat_r = self.master_dat_r,
            o_master_wb_sel = master_bus.sel,
            i_master_wb_ack = master_bus.ack,
            i_master_wb_err = master_bus.err,

            # LPC core signals
            o_lpc_data_out = self.lpc_data_out,
            i_lpc_data_in = self.lpc_data_in,
            o_lpc_data_direction = self.lpc_data_direction,
            o_lpc_irq_out = self.lpc_irq_out,
            i_lpc_irq_in = self.lpc_irq_in,
            o_lpc_irq_direction = self.lpc_irq_direction,
            i_lpc_frame_n = self.lpc_frame_n,
            i_lpc_reset_n = self.lpc_reset_n,
            i_lpc_clock = self.lpc_clock,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # Debug port
            o_debug_port = self.debug_port,
            o_lpc_clock_mirror = self.lpc_clock_mirror
        )
        platform.add_source("rtl/lpc_interface.v")

        # I/O drivers
        self.specials += SDRTristate(
            io  = pads.addrdata[0],
            o   = self.lpc_data_out[0],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[0],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[1],
            o   = self.lpc_data_out[1],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[1],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[2],
            o   = self.lpc_data_out[2],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[2],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[3],
            o   = self.lpc_data_out[3],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[3],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.serirq,
            o   = self.lpc_irq_out,
            oe  = self.lpc_irq_direction,
            i   = self.lpc_irq_in,
        )
        self.comb += self.lpc_frame_n.eq(pads.frame_n)
        self.comb += self.lpc_reset_n.eq(pads.reset_n)
        self.comb += self.lpc_clock.eq(pads.clk)

        if debug_signals is not None:
            self.comb += debug_signals[0].eq(self.debug_port[15])
            self.comb += debug_signals[1].eq(self.debug_port[14])
            self.comb += debug_signals[2].eq(self.debug_port[13])
            self.comb += debug_signals[3].eq(self.debug_port[12])
            self.comb += debug_signals[4].eq(self.debug_port[11])
            self.comb += debug_signals[5].eq(self.debug_port[10])
            self.comb += debug_signals[6].eq(self.debug_port[9])
            self.comb += debug_signals[7].eq(self.debug_port[8])

        if lpc_clk_mirror is not None:
            self.comb += lpc_clk_mirror.eq(self.lpc_clock_mirror)

        # Generate IRQ
        # Active high logic
        self.comb += self.ev.master_interrupt.trigger.eq(self.wb_irq)

# SPI interface ------------------------------------------------------------------------------------

from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

class TercelSPI(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq, endianness="big", adr_offset=0x0, debug_signals=None):
        self.bus     = bus     = wishbone.Interface(data_width=32, adr_width=30)
        self.cfg_bus = cfg_bus = wishbone.Interface(data_width=32, adr_width=30)
        self.cs_n    = cs_n    = Signal()

        # Bus endianness handlers
        self.dat_w = Signal(32)
        self.dat_r = Signal(32)
        self.comb += self.dat_w.eq(bus.dat_w if endianness == "big" else reverse_bytes(bus.dat_w))
        self.comb += bus.dat_r.eq(self.dat_r if endianness == "big" else reverse_bytes(self.dat_r))
        self.cfg_dat_w = Signal(32)
        self.cfg_dat_r = Signal(32)
        self.comb += self.cfg_dat_w.eq(cfg_bus.dat_w if endianness == "big" else reverse_bytes(cfg_bus.dat_w))
        self.comb += cfg_bus.dat_r.eq(self.cfg_dat_r if endianness == "big" else reverse_bytes(self.cfg_dat_r))

        # Calculate SPI flash address
        spi_bus_adr = Signal(30)
        self.comb += spi_bus_adr.eq(bus.adr - (adr_offset >> 2)) # wb address is in words, offset is in bytes

        # SPI bus signals
        self.spi_clock = Signal()
        self.spi_d0_out = Signal()
        self.spi_d0_direction = Signal()
        self.spi_d0_in = Signal()
        self.spi_d1_out = Signal()
        self.spi_d1_direction = Signal()
        self.spi_d1_in = Signal()
        self.spi_d2_out = Signal()
        self.spi_d2_direction = Signal()
        self.spi_d2_in = Signal()
        self.spi_d3_out = Signal()
        self.spi_d3_direction = Signal()
        self.spi_d3_in = Signal()
        self.spi_ss_n = Signal()

        # Debug signals
        self.debug_port = Signal(8)

        self.specials += Instance("tercel_spi_master_wishbone",
            # Configuration data
            i_sys_clk_freq = clk_freq,

            # Wishbone signals
            i_wb_cyc = bus.cyc,
            i_wb_stb = bus.stb,
            i_wb_we = bus.we,
            i_wb_addr = spi_bus_adr,  # The final SPI address is created inside the shim module from both wb_addr and wb_sel
            i_wb_dat_w = self.dat_w,
            o_wb_dat_r = self.dat_r,
            i_wb_sel = bus.sel,
            o_wb_ack = bus.ack,
            o_wb_err = bus.err,

            # Wishbone cfg port signals
            i_cfg_wb_cyc = cfg_bus.cyc,
            i_cfg_wb_stb = cfg_bus.stb,
            i_cfg_wb_we = cfg_bus.we,
            i_cfg_wb_addr = cfg_bus.adr,
            i_cfg_wb_dat_w = self.cfg_dat_w,
            o_cfg_wb_dat_r = self.cfg_dat_r,
            i_cfg_wb_sel = cfg_bus.sel,
            o_cfg_wb_ack = cfg_bus.ack,
            o_cfg_wb_err = cfg_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # Debug port
            o_debug_port = self.debug_port,

            # SPI interface
            o_spi_clock = self.spi_clock,
            o_spi_d0_out = self.spi_d0_out,
            o_spi_d0_direction = self.spi_d0_direction,
            i_spi_d0_in = self.spi_d0_in,
            o_spi_d1_out = self.spi_d1_out,
            o_spi_d1_direction = self.spi_d1_direction,
            i_spi_d1_in = self.spi_d1_in,
            o_spi_d2_out = self.spi_d2_out,
            o_spi_d2_direction = self.spi_d2_direction,
            i_spi_d2_in = self.spi_d2_in,
            o_spi_d3_out = self.spi_d3_out,
            o_spi_d3_direction = self.spi_d3_direction,
            i_spi_d3_in = self.spi_d3_in,
            o_spi_ss_n = self.spi_ss_n
        )
        platform.add_source("rtl/spi_master.v")

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.dq[0],
            o  = self.spi_d0_out,
            oe = self.spi_d0_direction,
            i  = self.spi_d0_in,
        )
        self.specials += SDRTristate(
            io = pads.dq[1],
            o  = self.spi_d1_out,
            oe = self.spi_d1_direction,
            i  = self.spi_d1_in,
        )
        self.specials += SDRTristate(
            io = pads.dq[2],
            o  = self.spi_d2_out,
            oe = self.spi_d2_direction,
            i  = self.spi_d2_in,
        )
        self.specials += SDRTristate(
            io = pads.dq[3],
            o  = self.spi_d3_out,
            oe = self.spi_d3_direction,
            i  = self.spi_d3_in,
        )
        self.comb += pads.cs_n.eq(self.spi_ss_n)
        self.comb += pads.clk.eq(self.spi_clock)

        if debug_signals is not None:
            self.comb += debug_signals[0].eq(self.spi_d0_in)
            self.comb += debug_signals[1].eq(self.spi_d1_in)
            self.comb += debug_signals[2].eq(self.spi_d2_in)
            self.comb += debug_signals[3].eq(self.spi_d3_in)
            #self.comb += debug_signals[2].eq(self.debug_port[0])
            #self.comb += debug_signals[3].eq(self.debug_port[1])
            #self.comb += debug_signals[4].eq(self.debug_port[2])
            #self.comb += debug_signals[5].eq(self.debug_port[3])

# FSI interface ------------------------------------------------------------------------------------

class OpenFSIMaster(Module, AutoCSR):
    def __init__(self, platform, pads, endianness="big", debug_pads=None):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        # FSI bus signals
        self.fsi_data_in = Signal()
        self.fsi_data_out = Signal()
        self.fsi_data_direction = Signal()
        self.fsi_clock = Signal()

        self.specials += Instance("fsi_master_litex_shim",
            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # FSI interface
            i_fsi_data_in = self.fsi_data_in,
            o_fsi_data_out = self.fsi_data_out,
            o_fsi_data_direction = self.fsi_data_direction,
            o_fsi_clock_out = self.fsi_clock
        )
        platform.add_source("rtl/fsi_master.v")

        self.fsi_data_tristate = Signal()
        self.comb += self.fsi_data_tristate.eq(~self.fsi_data_direction)
        self.specials += Instance("BB",
            io_B = pads.data,
            i_I = self.fsi_data_out,
            i_T = self.fsi_data_tristate,
            o_O = self.fsi_data_in
        )
        self.comb += pads.data_direction.eq(self.fsi_data_direction)
        self.comb += pads.clock.eq(self.fsi_clock)

        # Debug interface
        if (debug_pads is not None):
            self.comb += debug_pads.led_15.eq(self.fsi_clock)
            self.comb += debug_pads.led_14.eq(self.fsi_data_in)
            self.comb += debug_pads.led_13.eq(self.fsi_data_direction)

# I2C interface ------------------------------------------------------------------------------------

from litex.soc.interconnect.csr_eventmanager import EventManager, EventSourceLevel
from litex.soc.interconnect import wishbone, stream
from litex.build.io import SDRTristate

class OpenCoresI2CMaster(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq):
        self.bus    = bus    = wishbone.Interface(data_width=8, adr_width=5)
        self.wb_irq = Signal()

        # Set up IRQ handling
        self.submodules.ev = EventManager()
        self.ev.master_interrupt = EventSourceLevel(name="IRQ", description="I2C master core interrupt")
        self.ev.finalize()

        # I2C bus signals
        self.i2c_sda_out = Signal()
        self.i2c_sda_direction = Signal()
        self.i2c_sda_in = Signal()
        self.i2c_scl_out = Signal()
        self.i2c_scl_direction = Signal()
        self.i2c_scl_in = Signal()

        self.specials += Instance("i2c_master_top",
            # Configuration data
            i_sys_clk_freq = clk_freq,

            # Wishbone signals
            i_wb_cyc_i  = bus.cyc,
            i_wb_stb_i  = bus.stb,
            i_wb_we_i   = bus.we,
            i_wb_adr_i  = bus.adr,
            i_wb_dat_i  = bus.dat_w,
            o_wb_dat_o  = bus.dat_r,
            o_wb_ack_o  = bus.ack,
            o_wb_inta_o = self.wb_irq,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_arst_i   = ResetSignal('sys'),
            i_wb_rst_i = ResetSignal('sys'),
            i_wb_clk_i = ClockSignal('sys'),

            # I2C interface
            o_sda_pad_o   = self.i2c_sda_out,
            o_sda_padoe_o = self.i2c_sda_direction,
            i_sda_pad_i   = self.i2c_sda_in,
            o_scl_pad_o   = self.i2c_scl_out,
            o_scl_padoe_o = self.i2c_scl_direction,
            i_scl_pad_i   = self.i2c_scl_in
        )

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.sda,
            o  = self.i2c_sda_out,
            oe = self.i2c_sda_direction,
            i  = self.i2c_sda_in,
        )
        self.specials += SDRTristate(
            io = pads.scl,
            o  = self.i2c_scl_out,
            oe = self.i2c_scl_direction,
            i  = self.i2c_scl_in,
        )

        # Generate IRQ
        # Active high logic
        self.comb += self.ev.master_interrupt.trigger.eq(self.wb_irq)

# SimpleRTC Slave interface ------------------------------------------------------------------------------

from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes

class SimpleRTCSlave(Module, AutoCSR):
    def __init__(self, platform, endianness="big", rtc_clk_src="rtc", rtc_clk_freq=1e6):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.specials += Instance("simple_rtc_wishbone",
            # Parameters
            p_BASE_CLOCK_FREQUENCY_KHZ = int(rtc_clk_freq / 1000.0),

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),
            i_rtc_clock = ClockSignal(rtc_clk_src),
        )
        platform.add_source("rtl/simple_rtc.v")

# CRG ----------------------------------------------------------------------------------------------

class _CRG(Module):
    def __init__(self, platform, sys_clk_freq):
        self.clock_domains.cd_init    = ClockDomain()
        self.clock_domains.cd_por     = ClockDomain(reset_less=True)
        self.clock_domains.cd_sys     = ClockDomain()
        self.clock_domains.cd_sys2x   = ClockDomain()
        self.clock_domains.cd_sys2x_i = ClockDomain(reset_less=True)

        # # #

        self.stop  = Signal()
        self.reset = Signal()

        # Clk / Rst
        clk100 = platform.request("clk100")
        rst_n  = platform.request("rst_n")

        # Power on reset
        por_count = Signal(16, reset=2**16-1)
        por_done  = Signal()
        self.comb += self.cd_por.clk.eq(clk100)
        self.comb += por_done.eq(por_count == 0)
        self.sync.por += If(~por_done, por_count.eq(por_count - 1))

        # PLL
        self.submodules.pll = pll = ECP5PLL()
        pll.register_clkin(clk100, 100e6)
        pll.create_clkout(self.cd_sys2x_i, 2*sys_clk_freq)
        pll.create_clkout(self.cd_init, 25e6)
        self.specials += [
            Instance("ECLKSYNCB",
                i_ECLKI = self.cd_sys2x_i.clk,
                i_STOP  = self.stop,
                o_ECLKO = self.cd_sys2x.clk),
            Instance("CLKDIVF",
                p_DIV     = "2.0",
                i_ALIGNWD = 0,
                i_CLKI    = self.cd_sys2x.clk,
                i_RST     = self.reset,
                o_CDIVX   = self.cd_sys.clk),
            AsyncResetSynchronizer(self.cd_init,   ~por_done | ~pll.locked | ~rst_n),
            AsyncResetSynchronizer(self.cd_sys,    ~por_done | ~pll.locked | ~rst_n | self.reset),
            AsyncResetSynchronizer(self.cd_sys2x,  ~por_done | ~pll.locked | ~rst_n | self.reset),
        ]

# BaseSoC ------------------------------------------------------------------------------------------

class BaseSoC(SoCCore):
    mem_map = {
        "hostxicsicp"     : 0xc3ff0000,
        "hostxicsics"     : 0xc3ff1000,
        "hostspiflashcfg" : 0xc3ff7000,
        "simplertc"       : 0xc3ff8000,
        "openfsimaster"   : 0xc3ff9000,
        "i2cmaster1"      : 0xc3ffa000,
        "i2cmaster2"      : 0xc3ffa020,
        "i2cmaster3"      : 0xc3ffa040,
        "i2cmaster4"      : 0xc3ffa060,
        "hostspiflash"    : 0xc4000000,
        "hostlpcslave"    : 0xcb000000,
    }
    mem_map.update(SoCCore.mem_map)

    interrupt_map = {
        "hostlpcslave"  : 3,
        "openfsimaster" : 4,
        "i2cmaster1"    : 5,
        "i2cmaster2"    : 6,
        "i2cmaster3"    : 7,
        "i2cmaster4"    : 8,
    }
    interrupt_map.update(SoCCore.interrupt_map)

    def __init__(self, sys_clk_freq=int(75e6), device="LFE5UM5G", with_ethernet=False, with_openfsi_master=True, with_hostspiflash=True, with_hostlpcslave=True, with_i2c_masters=True, with_simple_rtc=True, toolchain="trellis", **kwargs):
        platform = versa_ecp5.Platform(toolchain=toolchain, device=device)

        # FIXME: adapt integrated rom size for Microwatt
        if kwargs.get("cpu_type", None) == "microwatt":
            kwargs["integrated_rom_size"] = 0xb000 if with_ethernet else 0x9000
        else:
            # There are numerous PowerPC-isms scattered throughout the HDL and firmware.
            # Even if you get a bistream out with a non-PowerPC CPU, it probably won't
            # work, and the firmware almost certainly won't build, let alone function.
            #
            # If you are an end user or software developer, you probably forgot to pass
            # "--cpu-type=microwatt" to the build script.
            #
            # If you are a developer and are trying to port the Kestrel HDL and firmware
            # to a non-PowerPC CPU, you probably know what you're doing and can debug
            # whatever you break on your own hardware.  Remove this line and keep hacking!
            raise OSError("Kestrel HDL and firmware currently require a PowerPC-compatible CPU to function.  Did you forget '--cpu-type=microwatt'?")

        # SoCCore -----------------------------------------_----------------------------------------
        SoCCore.__init__(self, platform, csr_data_width=32, irq_n_irqs=16, clk_freq=sys_clk_freq,
            ident          = "LiteX SoC on Versa ECP5",
            ident_version  = True,
            **kwargs)

        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, sys_clk_freq)

        # XICS -------------------------------------------------------------------------------------
        hostxicsicp_region = SoCRegion(origin=self.mem_map.get("hostxicsicp", None), size=4096, cached=False)
        hostxicsics_region = SoCRegion(origin=self.mem_map.get("hostxicsics", None), size=4096, cached=False)
        self.bus.add_slave(name="hostxicsicp", slave=self.cpu.xics.icp_bus, region=hostxicsicp_region)
        self.bus.add_slave(name="hostxicsics", slave=self.cpu.xics.ics_bus, region=hostxicsics_region)

        # DDR3 SDRAM -------------------------------------------------------------------------------
        if not self.integrated_main_ram_size:
            self.submodules.ddrphy = ECP5DDRPHY(
                platform.request("ddram"),
                sys_clk_freq=sys_clk_freq)
            self.add_csr("ddrphy")
            self.comb += self.crg.stop.eq(self.ddrphy.init.stop)
            self.comb += self.crg.reset.eq(self.ddrphy.init.reset)
            self.add_sdram("sdram",
                phy                     = self.ddrphy,
                module                  = MT41K64M16(sys_clk_freq, "1:2"),
                origin                  = self.mem_map["main_ram"],
                size                    = kwargs.get("max_sdram_size", 0x40000000),
                l2_cache_size           = kwargs.get("l2_size", 8192),
                l2_cache_min_data_width = kwargs.get("min_l2_data_width", 128),
                l2_cache_reverse        = True
            )

        # Ethernet ---------------------------------------------------------------------------------
        if with_ethernet:
            self.submodules.ethphy = LiteEthPHYRGMII(
                clock_pads = self.platform.request("eth_clocks"),
                pads       = self.platform.request("eth"))
            self.add_csr("ethphy")
            self.add_ethernet(phy=self.ethphy)

        # Debug pad locator
        debug2_pads = platform.request("debug_port_2")
        lpc_debug_mirror_clock_pad = platform.request("lpc_debug_mirror_clock")

        # Host SPI Flash (Tercel core) -------------------------------------------------------------
        if with_hostspiflash:
            hostspiflash4x_pads = platform.request("hostspiflash4x")
            self.host_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.hostspiflash   = TercelSPI(
                platform           = platform,
                pads               = hostspiflash4x_pads,
                debug_signals      = self.host_spi_dq_debug,
                clk_freq           = sys_clk_freq,
                endianness         = self.cpu.endianness,
                adr_offset         = self.mem_map.get("hostspiflash", None))
            self.add_csr("hostspiflash")
            hostspiflash_size      = 64*mB
            hostspiflash_region    = SoCRegion(origin=self.mem_map.get("hostspiflash", None), size=hostspiflash_size, cached=False)
            self.bus.add_slave(name="hostspiflash", slave=self.hostspiflash.bus, region=hostspiflash_region)
            hostspiflashcfg_size   = 128
            hostspiflashcfg_region = SoCRegion(origin=self.mem_map.get("hostspiflashcfg", None), size=hostspiflashcfg_size, cached=False)
            self.bus.add_slave(name="hostspiflashcfg", slave=self.hostspiflash.cfg_bus, region=hostspiflashcfg_region)

        # Host LPC Slave (Aquila core) -------------------------------------------------------------
        if with_hostlpcslave:
            hostlpcslave_pads = platform.request("hostlpcslave")
            self.host_lpc_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.host_lpc_clock_mirror = Signal()
            self.submodules.hostlpcslave   = AquilaLPCSlave(
                platform       = platform,
                pads           = hostlpcslave_pads,
                debug_signals  = self.host_lpc_debug,
                lpc_clk_mirror = self.host_lpc_clock_mirror,
                endianness     = self.cpu.endianness,
                adr_offset     = self.mem_map.get("hostlpcslave", None))
            self.add_csr("hostlpcslave")
            hostlpcslave_size   = 16*mB
            hostlpcslave_region = SoCRegion(origin=self.mem_map.get("hostlpcslave", None), size=hostlpcslave_size, cached=False)
            self.bus.add_slave(name="hostlpcslave", slave=self.hostlpcslave.slave_bus, region=hostlpcslave_region)
            self.bus.add_master(name="hostlpcslave", master=self.hostlpcslave.master_bus)

        # OpenFSI Master ---------------------------------------------------------------------------
        if with_openfsi_master:
            openfsi_master_pads = platform.request("openfsi_master")
            self.submodules.openfsi_master = OpenFSIMaster(
                platform        = platform,
                pads            = openfsi_master_pads,
                endianness      = self.cpu.endianness)
            self.add_csr("openfsimaster")
            openfsi_master_size = 128
            openfsi_master_region = SoCRegion(origin=self.mem_map.get("openfsimaster", None), size=openfsi_master_size, cached=False)
            self.bus.add_slave(name="openfsimaster", slave=self.openfsi_master.slave_bus, region=openfsi_master_region)

        # Debug hookups...
        #self.comb += debug2_pads.led_15.eq(self.host_spi_dq_debug[5])
        #self.comb += debug2_pads.led_14.eq(self.host_spi_dq_debug[4])
        #self.comb += debug2_pads.led_13.eq(hostspiflash4x_pads.clk)
        #self.comb += debug2_pads.led_12.eq(hostspiflash4x_pads.cs_n)
        #self.comb += debug2_pads.led_11.eq(self.host_spi_dq_debug[3])
        #self.comb += debug2_pads.led_10.eq(self.host_spi_dq_debug[2])
        #self.comb += debug2_pads.led_9.eq(self.host_spi_dq_debug[1])
        #self.comb += debug2_pads.led_8.eq(self.host_spi_dq_debug[0])

        self.comb += debug2_pads.led_15.eq(self.host_lpc_debug[7])
        self.comb += debug2_pads.led_14.eq(self.host_lpc_debug[6])
        self.comb += debug2_pads.led_13.eq(self.host_lpc_debug[5])
        self.comb += debug2_pads.led_12.eq(self.host_lpc_debug[4])
        self.comb += debug2_pads.led_11.eq(self.host_lpc_debug[3])
        self.comb += debug2_pads.led_10.eq(self.host_lpc_debug[2])
        self.comb += debug2_pads.led_9.eq(self.host_lpc_debug[1])
        self.comb += debug2_pads.led_8.eq(self.host_lpc_debug[0])
        self.comb += lpc_debug_mirror_clock_pad.eq(self.host_lpc_clock_mirror)

        # I2C Masters ------------------------------------------------------------------------------
        if with_i2c_masters:
            i2cmaster1_pads = platform.request("i2c_bus1_master")
            self.submodules.i2cmaster1 = OpenCoresI2CMaster(
                platform        = platform,
                pads            = i2cmaster1_pads,
                clk_freq        = sys_clk_freq)
            self.add_csr("i2cmaster1")
            i2cmaster1_size = 32
            i2cmaster1_region = SoCRegion(origin=self.mem_map.get("i2cmaster1", None), size=i2cmaster1_size, cached=False)
            self.bus.add_slave(name="i2cmaster1", slave=self.i2cmaster1.bus, region=i2cmaster1_region)

        if with_i2c_masters:
            i2cmaster2_pads = platform.request("i2c_bus2_master")
            self.submodules.i2cmaster2 = OpenCoresI2CMaster(
                platform        = platform,
                pads            = i2cmaster2_pads,
                clk_freq        = sys_clk_freq)
            self.add_csr("i2cmaster2")
            i2cmaster2_size = 32
            i2cmaster2_region = SoCRegion(origin=self.mem_map.get("i2cmaster2", None), size=i2cmaster2_size, cached=False)
            self.bus.add_slave(name="i2cmaster2", slave=self.i2cmaster2.bus, region=i2cmaster2_region)

        # Not needed for Blackbird / Talos II / Sparrowhawk; save space on the Versa board ECP5 -45 device
        #if with_i2c_masters:
            #i2cmaster3_pads = platform.request("i2c_bus3_master")
            #self.submodules.i2cmaster3 = OpenCoresI2CMaster(
                #platform        = platform,
                #pads            = i2cmaster3_pads,
                #clk_freq        = sys_clk_freq)
            #self.add_csr("i2cmaster3")
            #i2cmaster3_size = 32
            #i2cmaster3_region = SoCRegion(origin=self.mem_map.get("i2cmaster3", None), size=i2cmaster3_size, cached=False)
            #self.bus.add_slave(name="i2cmaster3", slave=self.i2cmaster3.bus, region=i2cmaster3_region)

        if with_i2c_masters:
            i2cmaster4_pads = platform.request("i2c_bus4_master")
            self.submodules.i2cmaster4 = OpenCoresI2CMaster(
                platform        = platform,
                pads            = i2cmaster4_pads,
                clk_freq        = sys_clk_freq)
            self.add_csr("i2cmaster4")
            i2cmaster4_size = 32
            i2cmaster4_region = SoCRegion(origin=self.mem_map.get("i2cmaster4", None), size=i2cmaster4_size, cached=False)
            self.bus.add_slave(name="i2cmaster4", slave=self.i2cmaster4.bus, region=i2cmaster4_region)

        if with_i2c_masters:
            platform.add_source("rtl/third_party/i2c_master/i2c_master_top.v")
            platform.add_source("rtl/third_party/i2c_master/i2c_master_bit_ctrl.v")
            platform.add_source("rtl/third_party/i2c_master/i2c_master_byte_ctrl.v")

        # SimpleRTC --------------------------------------------------------------------------------
        if with_simple_rtc:
            self.submodules.simple_rtc = SimpleRTCSlave(
                platform        = platform,
                endianness      = self.cpu.endianness,
                rtc_clk_src     = 'sys',
                rtc_clk_freq    = sys_clk_freq)
            self.add_csr("simplertc")
            simple_rtc_size = 128
            simple_rtc_region = SoCRegion(origin=self.mem_map.get("simplertc", None), size=simple_rtc_size, cached=False)
            self.bus.add_slave(name="simplertc", slave=self.simple_rtc.slave_bus, region=simple_rtc_region)

        # Discrete LEDs ----------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOTristate

        self.submodules.gpio1 = GPIOTristate(
            pads         = Cat(*[platform.request("user_led", i) for i in range(8)]))
        self.add_csr("gpio1")

        # DIP switches -------------------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOIn

        self.submodules.gpio2 = GPIOIn(
            signal       = Cat(*[platform.request("user_dip_btn", i) for i in range(8)]))
        self.add_csr("gpio2")

        # Alphanumeric display -----------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOTristate

        self.submodules.gpio3 = GPIOTristate(
            pads         = Cat(*[platform.request("alpha_led", i) for i in range(16)]))
        self.add_csr("gpio3")

# Build --------------------------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(description="LiteX SoC on Versa ECP5")
    parser.add_argument("--build", action="store_true", help="Build bitstream")
    parser.add_argument("--load",  action="store_true", help="Load bitstream")
    parser.add_argument("--toolchain", default="trellis", help="Gateware toolchain to use, trellis (default) or diamond")
    builder_args(parser)
    soc_sdram_args(parser)
    trellis_args(parser)
    parser.add_argument("--sys-clk-freq",  default=75e6,         help="System clock frequency (default=75MHz)")
    parser.add_argument("--device",        default="LFE5UM5G",   help="ECP5 device (LFE5UM5G (default) or LFE5UM)")
    parser.add_argument("--with-ethernet", action="store_true",  help="Enable Ethernet support")
    args = parser.parse_args()

    soc = BaseSoC(sys_clk_freq=int(float(args.sys_clk_freq)),
        device        = args.device,
        with_ethernet = args.with_ethernet,
        with_hostspiflash = True,
        with_hostlpcslave = True,
        with_i2c_masters = True,
        toolchain     = args.toolchain,
        **soc_sdram_argdict(args))

    builder = Builder(soc, **builder_argdict(args))
    builder_kargs = trellis_argdict(args) if args.toolchain == "trellis" else {}
    builder.build(**builder_kargs, run=args.build)

    if args.load:
        prog = soc.platform.create_programmer()
        prog.load_bitstream(os.path.join(builder.gateware_dir, soc.build_name + ".svf"))

if __name__ == "__main__":
    main()
